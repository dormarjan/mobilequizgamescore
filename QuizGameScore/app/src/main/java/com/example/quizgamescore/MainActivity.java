package com.example.quizgamescore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

public class MainActivity extends AppCompatActivity {

    EditText tUsername;
    EditText tEmail;
    EditText tPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        tUsername = (EditText) findViewById(R.id.etUsername);
        tEmail = (EditText) findViewById(R.id.etEmail);
        tPassword = (EditText) findViewById(R.id.etPassword);
    }

    public void onButtonClick(View view) {
        String finUsername = tUsername.getText().toString();
        String finEmail = tEmail.getText().toString();
        String finPassword = tPassword.getText().toString();

        Thread thread = new Thread(() -> {
            try {
                URL url = new URL("http://10.0.2.2:4000/quiz/User");
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("POST");
                connection.setRequestProperty("Content-Type", "application/json");
                connection.setRequestProperty("Accept", "application/json");
                connection.setDoOutput(true);

                JSONObject jsonInputString = new JSONObject();
                jsonInputString.put("Username", finUsername);
                jsonInputString.put("Email", finEmail);
                jsonInputString.put("Password", finPassword);

                Log.i("JSON", jsonInputString.toString());

                try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
                    outputStream.writeBytes(jsonInputString.toString());
                    outputStream.flush();
                }

                if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    Scanner scanner = new Scanner(connection.getInputStream());
                    StringBuilder response = new StringBuilder();

                    while (scanner.hasNext()) {
                        response.append(scanner.nextLine());
                    }
                    String respRaw = response.toString();
                    JSONObject resp = new JSONObject(respRaw);
                    String userID = resp.getString("id");

                    runOnUiThread(() -> {
                        Intent intent = new Intent(this, HomePageActivity.class);
                        intent.putExtra("userId", userID);
                        startActivity(intent);
                    });
                    System.out.println(response.toString());
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
        });

        thread.start();
    }

}